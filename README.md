# treatwell-at
A sample UI testing project using following tech stack:
* [Selenide](https://github.com/codeborne/selenide) - selenium wrapper
* [Guice](https://github.com/google/guice) - dependency injection
* [Cucumber](https://cucumber.io/) - BDD test specification
* [Gradle](https://github.com/gradle/gradle) - build tool

## Running tests from command line
Tests can be executed with following command.   
```bash
./gradlew clean test
```
Optional parameters:   
`-Pbrowser=$param`: browser to run tests in - `chrome`|`firefox`, `chrome` by default   
`-Pheadless=$param`: `true` to run ui tests in headless mode, `false` by default   
`-Ptags=$param`: cucumber tag expression - `@api`|`@ui`, all tests executed by default

