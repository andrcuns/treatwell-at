@api
Feature: Create user api

  Scenario: It is possible to create user with correct parameters
    When user is created via public api with parameters:
      | fullName         | Test user    |
      | password         | Password1234 |
      | newsletterSignup | false        |
    Then status code should be 200

  Scenario: Password too short error message is returned
    When user is created via public api with parameters:
      | fullName         | Test user |
      | password         | p123      |
      | newsletterSignup | false     |
    Then status code should be 400
    And response should contain 'Password is too short'

  Scenario: Same user is not created twice
    Given user is created via public api with parameters:
      | fullName         | Test user    |
      | password         | Password1234 |
      | newsletterSignup | false        |
    And status code is 200
    When trying to create user with existing data
    Then status code should be 200
    And response should contain 'authenticated'

  Scenario: Correct response in case mandatory field is missing
    When user is created via public api with parameters:
      | fullName         | Test user |
      | newsletterSignup | false     |
    Then status code should be 400
    And response should contain 'some of required attributes are not set [password]'
