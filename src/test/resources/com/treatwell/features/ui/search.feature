@ui
Feature: Search functionality tests

  Background: Open co.uk home page
    Given user opens 'http://www.treatwell.co.uk' page

  Scenario: Dropdown for treatment input appears
    When user enters 'Hair' as treatment search input
    Then first drop down item should contain 'Hair'

  Scenario: Dropdown for location input appears
    When user enters 'Birmingham' as area search input
    Then first drop down item should contain 'Birmingham'

  Scenario: Date and time drop down appears
    When user clicks on date and time input
    Then date and time input dialog should be visible

  Scenario: Search result page contains results
    When user fills in and submits search parameters:
      | treatment | Hair       |
      | location  | Birmingham |
      | date      | Tomorrow   |
      | time      | 12pm - 5pm |
    Then search results page and it's elements should be visible
    And search results page should have found venues

  Scenario: Search result page title reflects search parameters
    When user fills in and submits search parameters:
      | treatment | Hair       |
      | location  | Birmingham |
      | date      | Tomorrow   |
      | time      | 9am - 12pm |
    Then search results page and it's elements should be visible
    And search results page should contain treatment: Hair, location: Birmingham and tomorrow's date