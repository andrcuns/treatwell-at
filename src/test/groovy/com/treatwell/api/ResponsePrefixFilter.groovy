package com.treatwell.api

import io.restassured.builder.ResponseBuilder
import io.restassured.filter.Filter
import io.restassured.filter.FilterContext
import io.restassured.response.Response
import io.restassured.specification.FilterableRequestSpecification
import io.restassured.specification.FilterableResponseSpecification

class ResponsePrefixFilter implements Filter {

    @Override
    Response filter(FilterableRequestSpecification requestSpec, FilterableResponseSpecification responseSpec, FilterContext ctx) {
        final Response response = ctx.next(requestSpec, responseSpec)
        String strippedResponseBody = response
                .asString()
                .replace(")]}',", "")

        return new ResponseBuilder()
                .clone(response)
                .setBody(strippedResponseBody)
                .build()
    }
}
