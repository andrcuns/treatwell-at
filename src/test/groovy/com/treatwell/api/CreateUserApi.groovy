package com.treatwell.api

import com.google.inject.Singleton
import io.restassured.response.Response

import static groovy.json.JsonOutput.toJson

@Singleton
class CreateUserApi extends RequestSpecProvider {

    Response createUser(Map user) {
        Map marketingData = [
                marketing: [
                        sourceId   : "1",
                        sourceNotes: "source notes"
                ]]

        return baseRequest()
                .body(toJson(user + marketingData))
                .post("/me")
    }
}
