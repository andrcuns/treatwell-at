package com.treatwell.api

import io.restassured.RestAssured
import io.restassured.specification.RequestSpecification

import static io.restassured.config.LogConfig.logConfig
import static io.restassured.config.RestAssuredConfig.newConfig
import static io.restassured.config.SSLConfig.sslConfig
import static io.restassured.http.ContentType.JSON

class RequestSpecProvider {

    static {
        RestAssured.config = newConfig()
                .sslConfig(sslConfig().allowAllHostnames())
                .logConfig(logConfig()
                    .enablePrettyPrinting(true)
                    .enableLoggingOfRequestAndResponseIfValidationFails()
        )
    }

    protected RequestSpecification baseRequest() {
        RestAssured.with()
                .baseUri("https://www.treatwell.co.uk/api/v1")
                .accept(JSON)
                .contentType(JSON)
                .filter(new ResponsePrefixFilter())
    }
}
