package com.treatwell

import org.apache.commons.lang3.RandomStringUtils

class Utils {
    static String getRandomString(int length) {
        RandomStringUtils.random(length, true, true)
    }
}
