package com.treatwell.page

import com.codeborne.selenide.ElementsCollection
import com.codeborne.selenide.SelenideElement

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan
import static com.codeborne.selenide.Condition.text
import static com.codeborne.selenide.Condition.visible
import static com.codeborne.selenide.Selenide.$

class SearchResultPage {

    private SelenideElement title = $("[class=browse-header] [class=title]")

    private SelenideElement searchResultsContainer = $("[class=search-results-container]")
    private SelenideElement filterWrapper = searchResultsContainer.find("[class=filter-container]")

    private SelenideElement resultsContainer = searchResultsContainer.find("[class=results-container]")
    private ElementsCollection resultItems = resultsContainer.findAll("[class='results-item action-container ']")

    SearchResultPage pageWithElementsShouldBeVisible() {
        title.shouldBe(visible)
        filterWrapper.shouldBe(visible)
        resultsContainer.shouldBe(visible)
        return this
    }

    SearchResultPage pageShouldHaveSearchResults() {
        resultItems.shouldHave(sizeGreaterThan(0))
        return this
    }

    SearchResultPage pageTitleShouldContainTexts(String treatment, String location, String date) {
        title.shouldHave(text(treatment))
        title.shouldHave(text(location))
        title.shouldHave(text(date))
        return this
    }
}
