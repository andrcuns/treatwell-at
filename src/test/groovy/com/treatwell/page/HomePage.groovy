package com.treatwell.page

import static com.codeborne.selenide.Selenide.open

class HomePage {

    void openPage(String url = "http://www.treatwell.co.uk") {
        open(url)
    }
}
