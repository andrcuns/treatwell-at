package com.treatwell.page.modules

import com.codeborne.selenide.ElementsCollection
import com.codeborne.selenide.SelenideElement

import static com.codeborne.selenide.Condition.text
import static com.codeborne.selenide.Condition.visible
import static com.codeborne.selenide.Selectors.byText
import static com.codeborne.selenide.Selenide.$

class SearchModule {

    private SelenideElement searchTab = $("[class^=TreatmentSearchTab]")

    private SelenideElement treatmentInput = searchTab.find("[placeholder='Search for treatments']")
    private SelenideElement areaInput = searchTab.find("[placeholder='Enter postcode or area']")
    private SelenideElement dateInput = searchTab.find("[placeholder='Any Date']")

    private SelenideElement dropdownInputContainer = $("[class*=dropdownContainer]")
    private ElementsCollection listItems = dropdownInputContainer.findAll("[class^=ListItem]")

    private SelenideElement dateTimeDialog = $("[class^=DateTimeDialog--controls]")
    private SelenideElement confirmDateTimeButton = dateTimeDialog.find("[class^=DateTimeDialog--actions] [class^=Button]")

    private SelenideElement searchButton = searchTab.find(byText("Search Treatwell"))


    SearchModule enterTreatment(String treatment) {
        treatmentInput.click()
        treatmentInput.val(treatment)
        return this
    }

    SearchModule submitTreatment() {
        treatmentInput.pressEnter()
        return this
    }

    SearchModule enterArea(String area) {
        areaInput.click()
        areaInput.val(area)
        return this
    }

    SearchModule submitArea() {
        areaInput.pressEnter()
        return this
    }

    SearchModule dropDownContainerShouldBeVisible() {
        dropdownInputContainer.shouldBe(visible)
        return this
    }

    SearchModule dateTimeDialogShouldBeVisible() {
        dateTimeDialog.shouldBe(visible)
        return this
    }

    SearchModule firstSuggestionShouldContainText(String suggestedText) {
        listItems
                .first()
                .shouldHave(text(suggestedText))
        return this
    }

    SearchModule clickDateTimeInput() {
        dateInput.click()
        return this
    }

    SearchModule selectDateTime(String date, String time) {
        clickDateTimeInput()

        dateTimeDialog.find(byText(date)).click()
        dateTimeDialog.find(byText(time)).click()

        confirmDateTimeButton.click()
        return this
    }

    SearchModule submitSearchTab() {
        searchButton.click()
        return this
    }
}
