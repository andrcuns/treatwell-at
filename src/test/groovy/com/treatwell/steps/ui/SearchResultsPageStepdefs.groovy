package com.treatwell.steps.ui

import com.google.inject.Inject
import com.treatwell.page.SearchResultPage
import cucumber.api.java.en.Then

class SearchResultsPageStepdefs {

    @Inject
    private SearchResultPage searchResultPage

    @Then("search results page and it's elements should be visible")
    void searchResultsPageShouldBeVisible() {
        searchResultPage.pageWithElementsShouldBeVisible()
    }

    @Then("search results page should have found venues")
    void searchResultsShouldBePresent() {
        searchResultPage.pageShouldHaveSearchResults()
    }

    @Then("search results page should contain treatment: (.*), location: (.*) and (.*) date")
    void titleShouldContainTexts(String treatment, String location, String date) {
        String dateFormat = "dd MMMM yyyy"
        if (date.contains("today")) {
            date = new Date().format(dateFormat)
        } else if (date.contains("tomorrow")) {
            date = (new Date() + 1).format(dateFormat)
        }
        searchResultPage.pageTitleShouldContainTexts(treatment, location, date)
    }
}
