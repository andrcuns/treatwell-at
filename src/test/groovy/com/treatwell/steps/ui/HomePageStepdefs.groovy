package com.treatwell.steps.ui

import com.google.inject.Inject
import com.treatwell.page.HomePage
import com.treatwell.page.modules.SearchModule
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class HomePageStepdefs {

    @Inject
    private HomePage homePage

    @Inject
    private SearchModule searchModule

    @When("user opens '(.*)' page")
    void openHomePage(String url) {
        homePage.openPage(url)
    }

    @When("user enters '(.*)' (and submits it )?as treatment search input")
    void enterTreatment(String treatment, String submit) {
        searchModule.enterTreatment(treatment)
        if (submit) searchModule.submitTreatment()
    }

    @When("user enters '(.*)' (and submits it )?as area search input")
    void enterArea(String area, String submit) {
        searchModule.enterArea(area)
        if (submit) searchModule.submitArea()
    }

    @When("user clicks on date and time input")
    void clickDateTimeInput() {
        searchModule.clickDateTimeInput()
    }

    @When("^user selects date: '(Any date|Today|Tomorrow|Choose date...)' and time '(9am - 12pm|12pm - 5pm|5pm - Late)'\$")
    void enterDateAndTime(String date, String time) {
        searchModule.selectDateTime(date, time)
    }

    @When("user fills in and submits search parameters:")
    void fillAndSubmitSearchParameters(Map<String, String> searchParams) {
        enterTreatment(searchParams.treatment, "submit")
        enterArea(searchParams.location, "submit")
        enterDateAndTime(searchParams.date, searchParams.time)
        searchModule.submitSearchTab()
    }

    @Then("first drop down item should contain '(.*)'")
    void firstDropDownItemShouldContain(String text) {
        searchModule
                .dropDownContainerShouldBeVisible()
                .firstSuggestionShouldContainText(text)
    }

    @Then("date and time input dialog should be visible")
    void dateTimeDialogShouldBeVisible() {
        searchModule.dateTimeDialogShouldBeVisible()
    }
}
