package com.treatwell.steps

import cucumber.api.java.After

import static com.codeborne.selenide.WebDriverRunner.clearBrowserCache

class Hooks {

    @After("@ui")
    void cleanCache() {
        clearBrowserCache()
    }
}
