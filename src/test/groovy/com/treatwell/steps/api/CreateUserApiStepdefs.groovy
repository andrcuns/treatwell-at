package com.treatwell.steps.api

import com.google.inject.Inject
import com.treatwell.Utils
import com.treatwell.api.CreateUserApi
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import cucumber.runtime.java.guice.ScenarioScoped
import io.restassured.response.Response

import static org.hamcrest.Matchers.containsString

@ScenarioScoped
class CreateUserApiStepdefs {

    @Inject
    private CreateUserApi createUserApi

    private Response response
    private Map userData = [email: "automation+${Utils.getRandomString(8)}@treatwell.com"]

    @Given("user is created via public api with parameters:")
    void createUser(Map user) {
        userData += user
        response = createUserApi.createUser(userData)
    }

    @When("trying to create user with existing data")
    void createUserWIthSameData() {
        response = createUserApi.createUser(userData)
    }

    @Then("status code (?:should be|is) (\\d+)")
    void assertStatusCode(int statusCode) {
        response
                .then()
                .statusCode(statusCode)
    }

    @Then("response should contain '(.*)'")
    void assertErrorMessage(String string) {
        response
                .then()
                .assertThat().body(containsString(string))
    }
}
