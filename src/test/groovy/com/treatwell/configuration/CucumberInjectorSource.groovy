package com.treatwell.configuration

import com.google.inject.Guice
import com.google.inject.Injector
import com.google.inject.Stage
import com.treatwell.configuration.GuiceModule
import cucumber.api.guice.CucumberModules
import cucumber.runtime.java.guice.InjectorSource

class CucumberInjectorSource implements InjectorSource {
    @Override
    Injector getInjector() {
        return Guice.createInjector(Stage.PRODUCTION, CucumberModules.SCENARIO, new GuiceModule())
    }
}
