package com.treatwell.configuration

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import org.junit.runner.RunWith

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/com/treatwell/features",
        glue = "com.treatwell.steps",
        tags = "not @disabled",
        plugin = ["json:build/reports/cucumber-report/cucumber.json", "pretty"],
        strict = true
)
class CucumberRunner {
}
