package com.treatwell.configuration

import com.codeborne.selenide.Configuration

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver
import static io.github.bonigarcia.wdm.WebDriverManager.firefoxdriver
import static java.lang.System.getProperty

enum DriverSetupImplementation {
    CHROME{
        @Override
        void setBrowser() {
            chromedriver().setup()
            CONFIGURE_BROWSER.call("chrome")
        }
    },
    FIREFOX{
        @Override
        void setBrowser() {
            firefoxdriver().setup()
            CONFIGURE_BROWSER.call("firefox")
        }
    }

    private static final Closure CONFIGURE_BROWSER = { String browser ->
        Configuration.startMaximized = false
        Configuration.browserSize = "1024x760"
        Configuration.browser = browser
    }

    abstract void setBrowser()

    static void setUpWebBrowser() {
        DriverSetupImplementation browser = values().find { browser ->
            browser.name().equalsIgnoreCase(getProperty("browser", "chrome"))
        }
        browser.setBrowser()
    }
}
