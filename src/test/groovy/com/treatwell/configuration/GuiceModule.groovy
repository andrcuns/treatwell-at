package com.treatwell.configuration

import com.google.inject.AbstractModule
import org.slf4j.bridge.SLF4JBridgeHandler

import static com.treatwell.configuration.DriverSetupImplementation.setUpWebBrowser

class GuiceModule extends AbstractModule {

    @Override
    void configure() {
        routeJulToSlf()
        setUpWebBrowser()
    }

    private static void routeJulToSlf() {
        SLF4JBridgeHandler.removeHandlersForRootLogger()
        SLF4JBridgeHandler.install()
    }
}
